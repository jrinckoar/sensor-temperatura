# MONITOR DE TEMPERATURA

## Trabajo Final - Curso de Aplicaciones Móviles

### Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>

Descripción:

Este monitor de temperatura presenta una gráfica de la temperatura en función del tiempo y una gráfica de su
primer derivada. El dato de temperatura proviene de un sensor ds18b20 conectado a una Raspberry-Pi. La lectura
se realiza a través de un servidor Node.js cargado en la Raspberry.

- link para descargar el servidor: https://bitbucket.org/jrinckoar/servidor-monitor/src/master
