import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class SensorService {
  constructor(private httpClient: HttpClient) {}
  public serverUrl = 'http://192.168.0.14:3000';
  public timeStep = 0;
  public dlimit = 100;
  public data = [];
  public der_data = [];
  public labels = ['0'];
  public clock = 0;
  public temp_;
  public temp = 0;

  public getTemp() {
    // leer el dato de temperatura del servidor.
    return this.httpClient.get(this.serverUrl + '/');
  }

  public getLabel() {
    // calcular el tiempo, en minutos, perteneciente a la ultima lectura.
    this.clock = this.clock + this.timeStep / 60;
    // console.log(this.timeStep);
    // console.log(this.clock);
    return this.clock.toFixed(2).toString();
  }

  public UpdateTempData(timeStep) {
    this.timeStep = timeStep;
    // Verificar que el buffer de datos no supere el máximo
    if (this.data.length >= this.dlimit) {
      this.data = this.data.slice(1);
      this.labels = this.labels.slice(1);
    }
    // Obtener la lectura de la temperatura
    this.getTemp().subscribe((out) => {
      this.temp_ = out;
      this.temp = this.temp_;
    });
    // Agregar la temperatura al buffer de datos y su coordenada temporal
    this.data.push(this.temp);
    this.labels.push(this.getLabel());
    // Calcular derivada
    if (this.data.length >= 2) {
      const der =
        (this.data[this.data.length - 1] - this.data[this.data.length - 2]) /
        this.timeStep;
      this.der_data.push(der);
    }
    return {
      data: this.data, // Array de datos del sensor (temperatura).
      der: this.der_data, // Array de la primer derivada.
      labels: this.labels, // Array con el eje temporal.
      temp: this.temp, // Temperatura actual.
    };
  }
}
