import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
import { SensorService } from '../services/sensor.service';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  // @ViewChild(BaseChartDirective) chart: any;
  @ViewChild(BaseChartDirective) private _chart;

  public temp = 0;
  public timer;
  public bFlag = false;
  public Tmin = 60;
  public Tmax = 75;
  public GTmin = 0;
  public GTmax = 90;
  public GdTmin = -5;
  public GdTmax = 5;
  public timeStep = 1; // 1 seg.

  // Series del gráfico 1
  public lineChartData: ChartDataSets[] = [
    { data: [], label: 'Sensor A', fill: false },
    { data: [], label: 'T.Min', fill: false },
    { data: [], label: 'T.Max', fill: false },
  ];
  // Serie del gráfico 2
  public lineChartDataDer: ChartDataSets[] = [
    { data: [], label: 'Sensor A', fill: false },
  ];

  public lineChartLabels: Label[] = [];
  public lineChartColors: Color[] = [
    {
      borderColor: 'blue',
      backgroundColor: 'rgba(255,0,0,0.3)',
      pointBackgroundColor: '#039BE5',
    },
  ];

  // Opciones gráfico 1
  public lineChartOptions01: any = {
    responsive: true,
    animation: {
      duration: 0,
    },
    scales: {
      yAxes: [
        {
          scaleLabel: { display: true, labelString: 'T [ºC]' },
          ticks: {
            max: this.GTmax,
            min: this.GTmin,
          },
        },
      ],
      xAxes: [
        {
          scaleLabel: { display: true, labelString: 't [min]' },
        },
      ],
    },
  };

  // Opciones gráfico 2
  public lineChartOptions02: any = {
    responsive: true,
    animation: {
      duration: 0,
    },
    scales: {
      yAxes: [
        {
          scaleLabel: { display: true, labelString: 'dT / dt [ºC / min]' },
          ticks: {
            max: this.GdTmax,
            min: this.GdTmin,
          },
        },
      ],
      xAxes: [
        {
          scaleLabel: { display: true, labelString: 't [min]' },
        },
      ],
    },
  };

  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  constructor(private sensorSrv: SensorService) {}

  // Leer temperatura y actualizar datos para graficar
  public read_temp() {
    let out;
    out = this.sensorSrv.UpdateTempData(this.timeStep);
    // console.log(out);
    this.temp = out.temp;
    this.lineChartData[0].data = out.data;
    this.lineChartDataDer[0].data = out.der;
    this.lineChartLabels = out.labels;
    const a = Array(this.lineChartData[0].data.length).fill(this.Tmin);
    const b = Array(this.lineChartData[0].data.length).fill(this.Tmax);
    this.lineChartData[1].data = a;
    this.lineChartData[2].data = b;
  }

  // Comienza lectura cada timeStep
  public start_stop_reading() {
    this.bFlag = !this.bFlag;
    if (this.bFlag) {
      this.timer = setInterval(() => {
        this.read_temp();
      }, this.timeStep * 1000);
    } else {
      clearInterval(this.timer);
    }
  }
  // Verificar que Tmin < Tmax
  public OnEnterCheckTemp() {
    if (this.Tmin >= this.Tmax) {
      alert('Tmin >= Tmax');
      this.Tmin = 65;
      this.Tmax = 75;
    }
  }
  // Verificar tiempo de muestreo
  public OnEnterCheckTime() {
    if (this.timeStep <= 0) {
      alert('Tiempo de muestre0 <= 0');
      this.timeStep = 1;
    }
  }
  // Cambia el eje y del gráfico 1
  public OnEnterSetYaxis() {
    if (this.GTmin >= this.GTmax) {
      alert('y axis min >= y axis max');
      this.GTmin = 0;
      this.GTmax = 90;
    }
    this.lineChartOptions01.scales.yAxes[0].ticks.min = this.GTmin;
    this.lineChartOptions01.scales.yAxes[0].ticks.max = this.GTmax;
    this._chart.refresh();
  }
}
